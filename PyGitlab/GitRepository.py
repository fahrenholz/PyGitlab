import os
import subprocess
import datetime
import re
from .NotARepositoryError import NotARepositoryError


class GitRepository:
    """Handles a git repository"""

    def __init__(self, path=None):
        if path is None:
            self.__path = os.getcwd()
        else:
            self.__path = os.path.realpath(path)

        if not os.path.isdir(self.__path):
            raise NotADirectoryError
        if not os.path.isdir(self.__path + "/.git"):
            raise NotARepositoryError

    def tags(self, sort_asc=True):
        """Get all tags of repository"""
        cwd = os.getcwd()
        os.chdir(self.__path)
        command = ["git", "log", "--tags", "--simplify-by-decoration", "--pretty=%H | %D"]
        if not sort_asc:
            command.insert(2, "-r")
        emitted_tags = subprocess.run(command, stdout=subprocess.PIPE).stdout.decode("utf-8").split("\n")
        tags = []
        del emitted_tags[-1]
        for tag in emitted_tags:
            tag = tag.split(" | ")
            parsed_tag = {
                "hash": tag[0]
            }
            if tag[1].strip() == '':
                parsed_tag["tag"] = None
                parsed_tag["initial_commit"] = True
            else:
                tag[1] = tag[1].strip().split(', ')
                for info in tag[1]:
                    if "tag: " in info:
                        parsed_tag["tag"] = info.replace("tag: ", "")
                        parsed_tag["initial_commit"] = False
            tags.append(parsed_tag)
        os.chdir(cwd)

        return tags

    def history(self, to_hash="HEAD", from_hash="4b825dc642cb6eb9a060e54bf8d69288fbee4904"):
        cwd = os.getcwd()
        os.chdir(self.__path)
        """Get the history of changes between two commits"""
        command = ["git", "log", from_hash, to_hash, "--pretty=%H | %at | %s | %cN | %cE"]
        emitted_commits = subprocess.run(command, stdout=subprocess.PIPE).stdout.decode("utf-8").split("\n")
        history = []
        del emitted_commits[-1]
        for commit in emitted_commits:
            commit = commit.split(" | ")
            history_line = {
                "hash": commit[0].strip(),
                "datetime": datetime.datetime.fromtimestamp(int(commit[1].strip())),
                "subject": commit[2].strip(),
                "committer_name": commit[3].strip(),
                "committer_email": commit[4].strip()
            }
            history.append(history_line)
        os.chdir(cwd)

        return history

    def committers(self, to_hash="HEAD", from_hash="4b825dc642cb6eb9a060e54bf8d69288fbee4904"):
        """Get the committers between two hashes"""
        cwd = os.getcwd()
        os.chdir(self.__path)
        command = ["git", "log", from_hash, to_hash, "--pretty=%cN | %cE"]
        emitted_commits = subprocess.run(command, stdout=subprocess.PIPE).stdout.decode("utf-8").split("\n")
        committers = []
        del emitted_commits[-1]
        for commit in emitted_commits:
            commit = commit.strip().split(" | ")
            committer = {
                "committer_name": commit[0],
                "committer_email": commit[1]
            }
            committers.append(committer)
        committers = list({v['committer_email'] + " " + v['committer_email']: v for v in committers}.values())
        os.chdir(cwd)

        return committers

    def remotes(self):
        """Gets the remotes configured in the repository"""
        cwd = os.getcwd()
        os.chdir(self.__path)
        command = ["git", "remote", "-v"]
        emitted_remotes = subprocess.run(command, stdout=subprocess.PIPE).stdout.decode("utf-8").split("\n")
        remotes = {}
        del emitted_remotes[-1]
        for rem in emitted_remotes:
            rem = re.split("[\t\s]+", rem.strip())
            if rem[0].strip() not in remotes:
                remotes[rem[0].strip()] = {
                    "remote_name": rem[0].strip(),
                    "url": rem[1].strip(),
                    "access_for": [
                        rem[2].strip()[1:-1]
                    ]
                }
            else:
                remotes[rem[0].strip()]["access_for"].append(rem[2].strip()[1:-1])
        os.chdir(cwd)
        return list(remotes.values())

    def branches(self):
        """Gets the branches configured both remotely and locally in the repository"""
        cwd = os.getcwd()
        os.chdir(self.__path)
        command = ["git", "branch", "-a"]
        emitted_branches = subprocess.run(command, stdout=subprocess.PIPE).stdout.decode("utf-8").split("\n")
        del emitted_branches[-1]
        branches = {
            "local": [],
            "remote": [],
        }
        for bra in emitted_branches:
            bra = bra.split(" -> ")
            bra[0]= bra[0].replace("* ", "").replace("  ", "")
            branch = {
                "branch_name": bra[0],
                "is_symlink": True if len(bra) == 2 else False,
                "symlinks_to": bra[1] if len(bra) == 2 else None
            }
            if "remotes/" in bra[0]:
                branches["remote"].append(branch)
            else:
                branches["local"].append(branch)
        os.chdir(cwd)
        return branches

