import validators
import re
import requests
from urllib.parse import quote_plus
from .GitRepository import GitRepository
from .NoApiAccessError import NoApiAccessError


class GitlabRepository(GitRepository):
    """Handles a gitlab instance """

    def __init__(self, url, access_token=None, path=None, ssl_verify=True):
        """Constructor"""
        GitRepository.__init__(self, path=path)
        self.__url = url if url[-1] != "/" else url[0:-1]
        self.__ssl_verify = ssl_verify
        self.__access_token = access_token
        if not validators.url(self.__url):
            raise validators.utils.ValidationFailure
        regex = re.compile("(http[s]{0,1})://")
        self.__host = regex.sub("", self.__url)
        self.__proto = regex.match(self.__url).group(1)
        self.__ssl_verify = self.__ssl_verify if self.__proto == "https" else False
        self.__project_id = self.__determine_project_id()
        self.__api_url = self.__url + "/api/v4"
        self.__headers = {"Private-Token": self.__access_token} if self.__access_token is not None else {}
        self.__project = None
        self.__project_members = None
        self.__project_issues = None
        self.__project_labels = None
        self.__project_merge_requests = None
        self.__project_events = None

    def __determine_project_id(self):
        """Determines the gitlab-id of the project given all remotes and the provided Gitlab-URL"""
        gitlab_id = ""
        for remote in self.remotes():
            if self.__host in remote["url"]:
                gitlab_id = re.sub("https://|http://|git@", "", remote["url"]).replace(self.__host, "")[1:-4]

        return gitlab_id

    def project_id(self):
        """Returns the gitlab-project-id"""
        return self.__project_id

    def project(self):
        """Returns the gitlab-project"""
        if self.__project is None:
            self.__retrieve_project()

        project = self.__project.copy()
        project.pop("_links")
        return project

    def __retrieve_project(self, force_reload=False):
        """Retrieves a project from the API"""
        if self.__access_token is None or force_reload:
            raise NoApiAccessError
        self.__project = requests.get(self.__api_url + "/projects/" + quote_plus(self.__project_id),
                                      verify=self.__ssl_verify, headers=self.__headers).json()

    def project_members(self, force_reload=False):
        """Gives back the project_members"""
        if self.__project_members is None or force_reload:
            self.__project_members = self.__retrieve_project_subresources("members")

        return self.__project_members

    def project_issues(self, force_reload=False):
        """Gives back all issues of the project"""
        if self.__project_issues is None or force_reload:
            self.__project_issues = self.__retrieve_project_subresources("issues")

        return self.__project_issues

    def project_labels(self, force_reload=False):
        """Gives back all project labels"""
        if self.__project_labels is None or force_reload:
            self.__project_labels = self.__retrieve_project_subresources("labels")

        return self.__project_labels

    def project_merge_requests(self, force_reload=False):
        """Gives back all merge requests of the project"""
        if self.__project_merge_requests is None or force_reload:
            self.__project_merge_requests = self.__retrieve_project_subresources("merge_requests")

        return self.__project_merge_requests

    def project_events(self, force_reload=False):
        """Gives back all project events"""
        if self.__project_events is None or force_reload is True:
            self.__project_events = self.__retrieve_project_subresources("events")

        return self.__project_events

    def __retrieve_project_subresources(self, resource_pool=None):
        """Retrieves a subresource from the project API based on the project resources _links-collection"""
        if resource_pool is None:
            raise ValueError
        elif self.__access_token is None:
            raise NoApiAccessError
        elif self.__project is None:
            self.__retrieve_project()

        subresources = requests.get(self.__project["_links"][resource_pool], verify=self.__ssl_verify,
                                    headers=self.__headers).json()

        return subresources

