class NoApiAccessError(Exception):
    """Raised when no access token is given, but API access is requested"""
    def __init__(self):
        pass
