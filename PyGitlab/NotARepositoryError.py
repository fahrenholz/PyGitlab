class NotARepositoryError(OSError):
    """ Operation only works on repositories """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass
